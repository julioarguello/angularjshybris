import { Injectable } from '@angular/core';
import { Product } from '../data/product';
import { Http, Response } from '@angular/http';
import { Observable }     from 'rxjs/Observable';
import 'rxjs/Rx';

@Injectable()
export class ProductService {
 accessToken:string = "168cef06-f340-4a31-a8f3-ab27da766373";
 searchProductUrl:string = "https://localhost:9002/rest/v2/electronics/products/search?fields=FULL&pageSize=5&access_token="+this.accessToken;
 http : Http;

 constructor(http: Http){
       this.http = http;
 }


 getProducts() : Observable<Product[]> {
   return this.http.get(this.searchProductUrl)
                   .map(this.extractData);

  }

 private extractData(res: Response) : Product[] {
  let body = res.json();
  var products : Product[] = [];
  for (var item in body.products) {
    var product : Product = new Product();
    var productItem = body.products[item];
    product.code = productItem.code;
    product.name = productItem.name;
    product.description = productItem.description;
    product.price = productItem.price.value;
    product.summary = productItem.summary;
   if (productItem.images) {
     if (productItem.images.url) {
       product.imageUrl = productItem.images.url;
     }
    }
    product.url = productItem.url;
    products.push(product);
  }
  return products;
 }

 private handleError (error: any) {
  let errMsg = (error.message) ? error.message :
    error.status ? `${error.status} - ${error.statusText}` : 'Server error';
  console.error(errMsg); // log to console instead
  return Promise.reject(errMsg);
 }

}
