package org.training.storefront.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.pages.AbstractPageController;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.cms2.model.pages.ContentPageModel;
import de.hybris.platform.cms2.servicelayer.services.CMSPageService;
import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.servicelayer.user.impl.DefaultUserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UrlPathHelper;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * Created by Rauf_Aliev on 9/2/2016.
 */
@Controller
@RequestMapping(value = "/cmsservice")
public class CMSComponentServiceController extends AbstractPageController {

    private  UrlPathHelper urlPathHelper = new UrlPathHelper();

    @Resource
    CMSPageService cmsPageService;

    @RequestMapping(method = RequestMethod.GET)
    public String renderSlotById(final Model model, final HttpServletRequest request, final HttpServletResponse response,
                                   @RequestParam (value = "slotId", required = true) String slotId,
                                   @RequestParam (value = "pageId", required = true) String pageId
                                   )
            throws CMSItemNotFoundException {

        if (urlPathHelper == null) { urlPathHelper  = new UrlPathHelper(); }

        final ContentPageModel pageForRequest = getContentPageForRequest(pageId);
        response.setHeader("Access-Control-Allow-Origin", "*");
        if (pageForRequest != null)
        {
            storeCmsPageInModel(model, pageForRequest);
            setUpMetaDataForContentPage(model, pageForRequest);
            //model.addAttribute(WebConstants.BREADCRUMBS_KEY, contentPageBreadcrumbBuilder.getBreadcrumbs(pageForRequest));
            model.addAttribute("slotId", slotId);
            model.addAttribute("pageId", pageId);
            return "component";
        }


        return "component";
    }

    protected ContentPageModel getContentPageForRequest(String label)
    {

        try
        {
            // Lookup the CMS Content Page by label. Note that the label value must begin with a '/'.
            return cmsPageService.getPageForLabel(label);
        }
        catch (final CMSItemNotFoundException ignore)
        {
            // Ignore exception
        }
        return null;
    }

}